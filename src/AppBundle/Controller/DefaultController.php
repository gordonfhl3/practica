<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;  
use AppBundle\Entity\Product;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function homeAction(){
        return $this->render('home.html.twig');
    }

    /**
     * @Route("/addform", name="addform")
     */
    public function addFormAction(Request $request)
    {
    $product = new Product();

    $form = $this->createFormBuilder($product)
        ->add('name', TextType::class)
        ->add('price', TextType::class)
        ->add('description', TextType::class)
        ->add('save', SubmitType::class, array('label' => 'Añadir Producto'))
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $product = $form->getData();

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();

        return $this->redirectToRoute('success');
    
    }
    return $this->render('default/form.html.twig', array(
        'form' => $form->createView(),
    ));
    }

    /**
     * @Route("/readform", name="readform")
     */
    public function readFormAction(Request $request)
    {
    $productId = 0;

    $form = $this->createFormBuilder()
        ->add('id', IntegerType::class)
        ->add('save', SubmitType::class, array('label' => 'Buscar producto'))
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $productId = $form->getData();

        
        $product = $this->getDoctrine()->getRepository(Product::class)
            ->find($productId);
        if (!$product) {
        extract($productId);    
        throw $this->createNotFoundException(
                'No product found for id '.$id
             );
        }

        return new Response('Se ha hallado el producto con ID '.$product->getId(). ', nombre '.$product->getName(). ', precio '.$product->getPrice().', y descripcion '.$product->getDescription());
    
    }
    return $this->render('default/form.html.twig', array(
        'form' => $form->createView(),
    ));
    }

    /**
     * @Route("/update", name="update")
     */
    public function updateAction()
    {
    $entityManager = $this->getDoctrine()->getManager();
    $productId = 1;
    $product = $this->getDoctrine()
        ->getRepository(Product::class)
        ->find($productId);

    if (!$product) {
        throw $this->createNotFoundException(
            'No product found for id '.$productId
        );
    }

    $product->setName('New Keyboard');
    $entityManager->flush();

    return new Response('Se ha editado el producto con ID '.$product->getId(). ' y ahora posee un nuevo nombre: '.$product->getName());
    }

        /**
     * @Route("/updateform", name="updateform")
     */
    public function updateFormAction(Request $request)
    {

    $productId = 0;

    $form = $this->createFormBuilder()
        ->add('id', IntegerType::class)
        ->add('name', TextType::class)
        ->add('price', TextType::class)
        ->add('description', TextType::class)
        ->add('save', SubmitType::class, array('label' => 'Modificar Producto'))
        ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
        $productNew = $form->getData();

        extract($productNew);

        $product = $this->getDoctrine()->getRepository(Product::class)
            ->find($id);
        
        dump($name);

        $product->setName($name);
        $product->setPrice($price);
        $product->setDescription($description);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($product);
        $entityManager->flush();
        
            return $this->redirectToRoute('success');
            }
        return $this->render('default/form.html.twig', array(
            'form' => $form->createView(),
            ));
        }

    /**
     * @Route("/delete", name="delete")
     */
    public function deleteAction()
    {
    $entityManager = $this->getDoctrine()->getManager();
    $productId = 13;
    $product = $this->getDoctrine()
        ->getRepository(Product::class)
        ->find($productId);

    if (!$product) {
        throw $this->createNotFoundException(
            'No product found for id '.$productId
        );
    }
    $entityManager->remove($product);
    $entityManager->flush();

    return new Response('Se ha eliminado el producto con ID '.$productId);
    }

        /**
     * @Route("/deleteform", name="deleteform")
     */
    public function deleteFormAction(Request $request)
    {
        $productId = 0;

        $form = $this->createFormBuilder()
            ->add('id', IntegerType::class)
            ->add('save', SubmitType::class, array('label' => 'Eliminar Producto'))
            ->getForm();
    
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            $productId = $form->getData();
    
            
            $product = $this->getDoctrine()->getRepository(Product::class)
                ->find($productId);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($product);
            $entityManager->flush();
            extract($productId);
            return new Response('Se ha eliminado el producto con ID '.$id);
        }

        return $this->render('default/form.html.twig', array(
            'form' => $form->createView(),
            ));
    }

    /**
     * @Route("/success", name="success")
     */
    public function SuccessAction(){
        return $this->render('success.html.twig');
    }

}